FROM registry.gitlab.com/base-images3/alpine-base:latest

# Set environment variables for Java
ENV JAVA_HOME=/usr/lib/jvm/java-17-openjdk/jre
ENV PATH="$JAVA_HOME/bin:${PATH}"

ARG UID
ARG GID

# Install OpenJDK 17 JRE
RUN apk add --no-cache openjdk17-jre-headless

#adding non-root user
RUN adduser -D -u 1000 eqworker

WORKDIR /home/eqworker
USER eqworker

# Copy your application files to the container
# COPY your-app.jar /app/

# Specify the command to run your application (replace 'your-app.jar' with your actual JAR file)
# CMD ["java", "-jar", "your-app.jar"]
